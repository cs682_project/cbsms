<?php
include_once('main.php');
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <link rel="stylesheet" type="text/css" href="../../source/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="../../source/jquery/jquery.min.js"></script>
    <script src="../../source/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="JS/login_logout.js"></script>
</head>
<body>

<div class="divtopcorner">
    <img src="../../source/logo.png" height="150" width="150" alt="School Management System"/>
</div>

<ul>
    <li class="manulist">
        <a class="menulista btn" href="index.php">Home</a>
        <a class="menulista btn" href="updateTeacher.php">Update Profile</a>
        <a class="menulista btn" href="viewProfile.php">View Profile</a>
        <a class="menulista btn" href="course.php">Students Grade</a>
        <a class="menulista btn" href="courses.php">Courses</a>
        <a class="menulista btn" href="attendance.php">Attendance</a>
        <a class="menulista btn" href="exam.php">Exam Schedule</a>
        <a class="menulista btn" href="salary.php">Salary</a>
        <a class="menulista btn" href="report.php">Report</a>
        <a class="menulista btn" href="searchStudent.php">Search Portal</a>
    </li>
</ul>
<div align="center">
    <h4>Hi! <?php echo $check . " "; ?></h4>
    <a class="menulista btn" href="logout.php" onmouseover="changemouseover(this);"
       onmouseout="changemouseout(this,'<?php echo ucfirst($loged_user_name); ?>');"><?php echo "Logout"; ?></a>
</div>
<hr/>
</body>
</html>
