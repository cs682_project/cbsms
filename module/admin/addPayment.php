<?php
include_once('main.php');
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <link rel="stylesheet" type="text/css" href="../../source/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="../../source/jquery/jquery.min.js"></script>
    <script src="../../source/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="JS/login_logout.js"></script>
</head>
<body>

<div class="divtopcorner">
    <img src="../../source/logo.png" height="150" width="150" alt="School Management System"/>
</div>

<ul>
    <li class="manulist">
        <a class="menulista btn" href="index.php">Home</a>
        <a class="menulista btn" href="manageStudent.php">Manage Student</a>
        <a class="menulista btn" href="manageTeacher.php">Manage Teacher</a>
        <a class="menulista btn" href="manageParent.php">Manage Parent</a>

        <a class="menulista btn" href="course.php">Course</a>
        <a class="menulista btn" href="attendance.php">Attendance</a>
        <a class="menulista btn" href="examSchedule.php">Exam Schedule</a>
        <a class="menulista btn" href="salary.php">Salary</a>
        <a class="menulista btn" href="report.php">Report</a>
        <a class="menulista btn" href="payment.php">Payment</a>

    </li>
</ul>
<div align="center">
    <h4>Hi! Admin <?php echo $check . " "; ?></h4>
    <a class="menulista btn" href="logout.php" onmouseover="changemouseover(this);"
       onmouseout="changemouseout(this,'<?php echo ucfirst($loged_user_name); ?>');"><?php echo "Logout"; ?></a>
</div>
<hr/>
<center>
    <h1>Student Tuition Fees</h1>
    <form action="#" method="post">
        <table class='table table-bordered' cellpadding="6">
            <tr>
                <td>Student ID:</td>
                <td><input class='form-control' type="text" name="id" placeholder="Enter Student Id."></td>
            </tr>
            <tr>
                <td>Amount:</td>
                <td><input class='form-control' type="text" name="amount" placeholder="Enter Amount."></td>
            </tr>
            <tr>
                <td>Month:</td>
                <td><input class='form-control' type="text" name="month" placeholder="Enter Month.(April as 4)"></td>
            </tr>
            <tr>
                <td>Year:</td>
                <td><input class='form-control' type="text" name="year" placeholder="Enter Year.(2016)"></td>
            </tr>
            <tr>
                <td></td>
                <td><input class='form-control btn btn-warning' type="submit" name="submit" value="Submit"></td>
            </tr>
        </table>
    </form>
</center>
</body>
</html>
<?php
include_once('../../service/mysqlcon.php');
if (!empty($_POST['submit'])) {
    $id = $_POST['id'];
    $amount = $_POST['amount'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    $sql = "INSERT INTO payment VALUES(null,'$id','$amount','$month','$year')";
    $success = mysql_query($sql, $link);
    if (!$success) {
        die('Could not enter data: ' . mysql_error());
    }
    echo "Transaction successfull\n";
}
?>
