<?php
include_once('main.php');
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <link rel="stylesheet" type="text/css" href="../../source/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="../../source/jquery/jquery.min.js"></script>
    <script src="../../source/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="JS/login_logout.js"></script>
</head>
<body>

<div class="divtopcorner">
    <img src="../../source/logo.png" height="150" width="150" alt="School Management System"/>
</div>

<ul>
    <li class="manulist">
        <a class="menulista btn" href="index.php">Home</a>
        <a class="menulista btn" href="manageStudent.php">Manage Student</a>
        <a class="menulista btn" href="manageTeacher.php">Manage Teacher</a>
        <a class="menulista btn" href="manageParent.php">Manage Parent</a>

        <a class="menulista btn" href="course.php">Course</a>
        <a class="menulista btn" href="attendance.php">Attendance</a>
        <a class="menulista btn" href="examSchedule.php">Exam Schedule</a>
        <a class="menulista btn" href="salary.php">Salary</a>
        <a class="menulista btn" href="report.php">Report</a>
        <a class="menulista btn" href="payment.php">Payment</a>

    </li>
</ul>
<div align="center">
    <h4>Hi! Admin <?php echo $check . " "; ?></h4>
    <a class="menulista btn" href="logout.php" onmouseover="changemouseover(this);"
       onmouseout="changemouseout(this,'<?php echo ucfirst($loged_user_name); ?>');"><?php echo "Logout"; ?></a>
</div>
<hr/>
<center>
    <h2>Exam Schedule List</h2>
    <form action="#" method="post">
        <table class='table table-bordered' cellpadding="6">
            <tr>
                <td>Exam Schedule Id:</td>
                <td><input class='form-control' type="text" name="id" placeholder="Exam Schedule ID"></td>
            </tr>
            <tr>
                <td>Exam Date:</td>
                <td><input class='form-control' type="text" name="examDate" placeholder="Exam Date(y-m-d)"></td>
            </tr>
            <tr>
                <td>Exam Time:</td>
                <td><input class='form-control' type="text" name="examTime" placeholder="Exam Time(H:M - H:M)"></td>
            </tr>
            <tr>
                <td>Course ID:</td>
                <td><input class='form-control' type="text" name="courseId" placeholder="Course ID"></td>
            </tr>
            <tr>
                <td></td>
                <td><input class='form-control btn btn-warning' type="submit" name="submit" value="Submit"></td>
            </tr>
        </table>
    </form>
</center>
</body>
</html>
<?php
include_once('../../service/mysqlcon.php');
if (!empty($_POST['submit'])) {
    $id = $_POST['id'];
    $examDate = $_POST['examDate'];
    $examTime = $_POST['examTime'];
    $courseId = $_POST['courseId'];
    $sql = "INSERT INTO examschedule VALUES('$id','$examDate','$examTime','$courseId')";
    $success = mysql_query($sql, $link);
    if (!$success) {
        die('Could not enter data: ' . mysql_error());
    }
    echo "Entered data successfully\n";
}
?>
