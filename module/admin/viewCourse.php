<?php
include_once('main.php');
include_once('../../service/mysqlcon.php');
$sql = "SELECT * FROM course;";
$res = mysql_query($sql);
$string = "";
while ($row = mysql_fetch_array($res)) {
    $string .= '<tr><td>' . $row['id'] . '</td><td>' . $row['name'] .
        '</td><td>' . $row['teacherid'] . '</td><td>' . $row['studentid'] .
        '</td><td>' . $row['classid'] . '</td></tr>';
}
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <link rel="stylesheet" type="text/css" href="../../source/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="../../source/jquery/jquery.min.js"></script>
    <script src="../../source/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="JS/login_logout.js"></script>
    <script src="JS/searchCourse.js"></script>
</head>
<body>

<div class="divtopcorner">
    <img src="../../source/logo.png" height="150" width="150" alt="School Management System"/>
</div>

<ul>
    <li class="manulist">
        <a class="menulista btn" href="index.php">Home</a>
        <a class="menulista btn" href="manageStudent.php">Manage Student</a>
        <a class="menulista btn" href="manageTeacher.php">Manage Teacher</a>
        <a class="menulista btn" href="manageParent.php">Manage Parent</a>

        <a class="menulista btn" href="course.php">Course</a>
        <a class="menulista btn" href="index.php">Attendance</a>
        <a class="menulista btn" href="index.php">Exam Schedule</a>
        <a class="menulista btn" href="index.php">Salary</a>
        <a class="menulista btn" href="index.php">Report</a>
        <a class="menulista btn" href="index.php">Payment</a>

    </li>
</ul>
<div align="center">
    <h4>Hi! Admin <?php echo $check . " "; ?></h4>
    <a class="menulista btn" href="logout.php" onmouseover="changemouseover(this);"
       onmouseout="changemouseout(this,'<?php echo ucfirst($loged_user_name); ?>');"><?php echo "Logout"; ?></a>
</div>
<hr/>
<center>
    <table class='table table-bordered'>
        <tr>
            <td><b>Search By Id Or Name: </b></td>
            <td><input class='form-control' type="text" name="searchId" placeholder="Search By Id Or Name:"
                       onkeyup="getCourse(this.value);">
            </td>
        </tr>
    </table>
</center>
<br/>
<center><h2>Course List</h2></center>
<center>
    <table class='table table-bordered' border="1" id='courseList'>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Teacher ID</th>
            <th>Student ID Name</th>
            <th>Class ID</th>
        </tr>
        <?php echo $string; ?>
    </table>
</center>
</body>
</html>
