<?php
include_once('main.php');
include_once('../../service/mysqlcon.php');
$string = "<tr>
    <th>Teacher</th>
    <th>Course Id</th>
    <th>Class</th>
    <th>#OF Students</th>
    </tr>";
$sql = "select t.name as teacher,ac.name as course,ac.classid as class,count(g.id) as no_of_std from teachers t,takencoursebyteacher tc,availablecourse ac,grade g where t.id=tc.teacherid and ac.id=tc.courseid and tc.courseid=g.courseid and g.grade not in('A+','A','A-','B+','B','B-')group by ac.id";
$res = mysql_query($sql);
while ($row = mysql_fetch_array($res)) {
    $string .= "<tr><td>" . $row['teacher'] . "</td><td>" . $row['course'] . "</td><td>" . $row['class'] .
        "</td><td>" . $row['no_of_std'] . "</td></tr>";
}
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <link rel="stylesheet" type="text/css" href="../../source/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="../../source/jquery/jquery.min.js"></script>
    <script src="../../source/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="JS/login_logout.js"></script>
</head>
<body>

<div class="divtopcorner">
    <img src="../../source/logo.png" height="150" width="150" alt="School Management System"/>
</div>

<ul>
    <li class="manulist">
        <a class="menulista btn" href="index.php">Home</a>
        <a class="menulista btn" href="manageStudent.php">Manage Student</a>
        <a class="menulista btn" href="manageTeacher.php">Manage Teacher</a>
        <a class="menulista btn" href="manageParent.php">Manage Parent</a>

        <a class="menulista btn" href="course.php">Course</a>
        <a class="menulista btn" href="attendance.php">Attendance</a>
        <a class="menulista btn" href="examSchedule.php">Exam Schedule</a>
        <a class="menulista btn" href="salary.php">Salary</a>
        <a class="menulista btn" href="report.php">Report</a>
        <a class="menulista btn" href="payment.php">Payment</a>

    </li>
</ul>
<div align="center">
    <h4>Hi! Admin <?php echo $check . " "; ?></h4>
    <a class="menulista btn" href="logout.php" onmouseover="changemouseover(this);"
       onmouseout="changemouseout(this,'<?php echo ucfirst($loged_user_name); ?>');"><?php echo "Logout"; ?></a>
</div>
<hr/>
<center>
    <h1>Teacher Evaluation</h1>
    <table class='table table-bordered' border="1">
        <?php echo $string; ?>
    </table>
</center>
</body>
</html>
