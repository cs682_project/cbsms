<?php
include_once('main.php');
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <link rel="stylesheet" type="text/css" href="../../source/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="../../source/jquery/jquery.min.js"></script>
    <script src="../../source/bootstrap/js/bootstrap.min.js"></script>
    <script src="JS/login_logout.js"></script>
</head>
<body>
<div class="divtopcorner">
    <img src="../../source/logo.png" height="150" width="150" alt="School Management System"/>
</div>

<ul>
    <li class="manulist">
        <a class="menulista btn btn-primary" href="index.php">Home</a>
        <a class="menulista btn btn-primary" href="manageStudent.php">Manage Student</a>
        <a class="menulista btn btn-primary" href="manageTeacher.php">Manage Teacher</a>
        <a class="menulista btn btn-primary" href="manageParent.php">Manage Parent</a>
        <a class="menulista btn btn-primary" href="course.php">Course</a>
        <a class="menulista btn btn-primary" href="attendance.php">Attendance</a>
        <a class="menulista btn btn-primary" href="examSchedule.php">Exam Schedule</a>
        <a class="menulista btn btn-primary" href="salary.php">Salary</a>
        <a class="menulista btn btn-primary" href="report.php">Report</a>
        <a class="menulista btn btn-primary" href="payment.php">Payment</a>

    </li>
</ul>
<div align="center">
    <h4>Hi! Admin <?php echo $check . " "; ?></h4>
    <a class="menulista btn btn-primary" href="logout.php" onmouseover="changemouseover(this);"
       onmouseout="changemouseout(this,'<?php echo ucfirst($loged_user_name); ?>');"><?php echo "Logout"; ?></a>
</div>
<hr/>
</body>
</html>
