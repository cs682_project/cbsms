<?php
include_once('main.php');

?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <link rel="stylesheet" type="text/css" href="../../source/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script src="../../source/jquery/jquery.min.js"></script>
    <script src="../../source/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../source/CSS/style.css">
    <script type="text/javascript" src="jquery-1.12.3.js"></script>
    <script type="text/javascript" src="Attendance.js"></script>
    <script src="JS/login_logout.js"></script>
</head>
<body onload="ajaxRequestToGetAttendancePresentThisMonth();">


<div class="divtopcorner">
    <img src="../../source/logo.png" height="150" width="150" alt="School Management System"/>
</div>

<ul>
    <li class="manulist">
        <a class="menulista btn" href="index.php">Home</a>
        <a class="menulista btn" href="modify.php">Change Password</a>
        <a class="menulista btn" href="checkchild.php">Childs Information</a>
        <a class="menulista btn" href="childcourse.php">Childs Course And Result</a>
        <a class="menulista btn" href="childpayment.php">Child Payments</a>
        <a class="menulista btn" href="childattendance.php">Childs Attendance</a>
        <a class="menulista btn" href="childreport.php">Childs Report</a>

        <div align="center">
            <h4>Hi! Parents <?php echo $check . " "; ?> </h4>
            <a class="menulista btn" href="logout.php" onmouseover="changemouseover(this);"
               onmouseout="changemouseout(this,'<?php echo ucfirst($loged_user_name); ?>');"><?php echo "Logout"; ?></a>
        </div>


    </li>
</ul>
<hr/>
<div align="center" style="background-color:white;">

    Select your Child:<select class='form-control' id="childid" name="childid"
                              onchange="ajaxRequestToGetAttendancePresentThisMonth();"
                              style="background-color:white;"><?php


        $classget = "SELECT  * FROM students where parentid='$check'";
        $res = mysql_query($classget);

        while ($cln = mysql_fetch_array($res)) {

            echo '<option value="', $cln['id'], '" >', $cln['name'], '</option>';

        }

        ?>

    </select>
    <hr/>

    Select Attendance that your child present: Current Month:<input class='form-control' type="radio"
                                                                    onclick="ajaxRequestToGetAttendancePresentThisMonth();"
                                                                    value="thismonth" id="present" name="present"
                                                                    checked="checked"/> ALL : <input
        class='form-control' type="radio"
        onclick="ajaxRequestToGetAttendancePresentAll();"
        value="all"
        id="present"
        name="present"/>
</div>
<hr/>
<div align="center">
    <table class='table table-bordered' id="mypresent" border="1">

    </table>
</div>
<hr/>
<div align="center" style="background-color:white;">

    Select Attendance that your child are absent : Current Month:<input class='form-control' type="radio"
                                                                        onclick="ajaxRequestToGetAttendanceAbsentThisMonth();"
                                                                        value="thismonth" id="absent" name="absent"
                                                                        checked="checked"/> ALL : <input
        class='form-control' type="radio"
        onclick="ajaxRequestToGetAttendanceAbsentAll();"
        value="all"
        id="absent"
        name="absent"/>
</div>
<hr/>
<div align="center">
    <table class='table table-bordered' id="myabsent" border="1">

    </table>
</div>


</body>
</html>