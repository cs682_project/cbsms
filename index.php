<?php
$login_code = isset($_REQUEST['login']) ? $_REQUEST['login'] : '1';
if ($login_code == "false") {
    $login_message = "Wrong Credentials !";
    $color = "red";
} else {
    $login_message = "Please Login !";
    $color = "green";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="source/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="source/CSS/style.css">
    <script src="source/jquery/jquery.min.js"></script>
    <script src="source/bootstrap/js/bootstrap.min.js"></script>
    <script src="source/js/loginValidate.js"></script>
    <title>School Management System</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12 text-xs-center">
            <img src="source/logo.png" width="400" height="230"/>
        </div>
    </div>
    <div class="row text-xs-center">
        <?php echo "<font  color='$color'>$login_message</font>"; ?>
    </div>
    <div class="row">
        <div class="col-lg-offset-3 col-sm-6">
            <form class="form-horizontal" onsubmit="return loginValidate();" method="post"
                  action="service/check.access.php">
                <div class="form-group row">
                    <label class="control-label col-sm-2" for="myid">User Id:</label>
                    <div class="col-sm-10">
                        <input class='form-control' type="id" class="form-control" id="myid" name="myid" autofocus=""
                               placeholder="Enter Id">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-2" for="mypassword">Password:</label>
                    <div class="col-sm-10">
                        <input class='form-control' type="password" class="form-control" id="mypassword"
                               name="mypassword"
                               placeholder="Enter password">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label><input type="checkbox"> Remember me</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
